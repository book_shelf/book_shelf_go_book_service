package client

import (
	"book_shelf/book_shelf_go_book_service.git/config"
)

type ServiceManagerI interface{}

type grpcClients struct{}

func NewGrpcClients(cfg config.Config) (ServiceManagerI, error) {

	return &grpcClients{}, nil
}
