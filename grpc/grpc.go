package grpc

import (
	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"

	"book_shelf/book_shelf_go_book_service.git/config"
	"book_shelf/book_shelf_go_book_service.git/genproto/book_service"
	"book_shelf/book_shelf_go_book_service.git/grpc/client"
	"book_shelf/book_shelf_go_book_service.git/grpc/service"
	"book_shelf/book_shelf_go_book_service.git/pkg/logger"
	"book_shelf/book_shelf_go_book_service.git/storage"
)

func SetUpServer(cfg config.Config, log logger.LoggerI, strg storage.StorageI, srvc client.ServiceManagerI) (grpcServer *grpc.Server) {

	grpcServer = grpc.NewServer()

	book_service.RegisterBookServiceServer(grpcServer, service.NewBookService(cfg, log, strg, srvc))

	reflection.Register(grpcServer)
	return
}
