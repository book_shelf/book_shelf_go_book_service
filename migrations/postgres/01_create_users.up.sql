
CREATE TABLE IF NOT EXISTS "books" (
    "book_id" uuid PRIMARY KEY,
    "isbn" VARCHAR NOT NULL,
    "title" VARCHAR NOT NULL,
    "cover" VARCHAR NOT NULL,
    "author" VARCHAR NOT NULL,
    "published" DATE NOT NULL,
    "pages" INT NOT NULL,
    "status" INT DEFAULT 0,     --  0- new  1-reading   2-finished
    "created_at" TIMESTAMP DEFAULT (now()),
    "updated_at" TIMESTAMP DEFAULT (now()),
    "deleted_at" TIMESTAMP
);
