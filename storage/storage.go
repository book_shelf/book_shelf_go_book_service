package storage

import (
	"context"
	"book_shelf/book_shelf_go_book_service.git/genproto/book_service"
	"book_shelf/book_shelf_go_book_service.git/models"
)

type StorageI interface {
	CloseDB()
	Book() BookRepoI
}


type BookRepoI interface {
	Create(ctx context.Context, req *book_service.CreateBook) (resp *book_service.BookPrimaryKey, err error)
	GetByPKey(ctx context.Context, req *book_service.BookPrimaryKey) (resp *book_service.Book, err error)
	GetAll(ctx context.Context, req *book_service.GetListBookRequest) (resp *book_service.GetListBookResponse, err error)
	Update(ctx context.Context, req *book_service.UpdateBook) (rowsAffected int64, err error)
	UpdatePatch(ctx context.Context, req *models.UpdatePatchRequest) (rowsAffected int64, err error)
	Delete(ctx context.Context, req *book_service.BookPrimaryKey) error
}
