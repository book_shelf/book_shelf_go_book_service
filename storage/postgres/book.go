package postgres

import (
	"context"
	"errors"
	"fmt"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v4/pgxpool"

	"book_shelf/book_shelf_go_book_service.git/genproto/book_service"
	"book_shelf/book_shelf_go_book_service.git/models"
	"book_shelf/book_shelf_go_book_service.git/pkg/helper"
	"book_shelf/book_shelf_go_book_service.git/storage"
)

type BookRepo struct {
	db *pgxpool.Pool
}

func NewBookRepo(db *pgxpool.Pool) storage.BookRepoI {
	return &BookRepo{
		db: db,
	}
}

func (c *BookRepo) Create(ctx context.Context, req *book_service.CreateBook) (resp *book_service.BookPrimaryKey, err error) {

	var id = uuid.New()

	query := `INSERT INTO "books" (
				book_id,
				isbn,
				title,
				cover,
				author,
				published,
				pages,
				updated_at
			) VALUES ($1, $2, $3, $4, $5, $6, $7, now())
		`

	_, err = c.db.Exec(ctx,
		query,
		id.String(),
		req.Isbn,
		req.Title,
		req.Cover,
		req.Author,
		req.Published,
		req.Pages,
	)

	if err != nil {
		return nil, err
	}

	return &book_service.BookPrimaryKey{BookId: id.String()}, nil
}

func (c *BookRepo) GetByPKey(ctx context.Context, req *book_service.BookPrimaryKey) (*book_service.Book, error) {

	query := `
		SELECT
			book_id,
			isbn,
			title,
			cover,
			author,
			CAST(published AS VARCHAR),
			pages,
			COALESCE(status, 0),
			TO_CHAR(created_at, 'YYYY-MM-DD HH24:MI:SS'),
			TO_CHAR(updated_at, 'YYYY-MM-DD HH24:MI:SS'),
			COALESCE(TO_CHAR(deleted_at, 'YYYY-MM-DD HH24:MI:SS'), '')
		FROM "books"
		WHERE book_id = $1 AND deleted_at is null
	`
	var resp book_service.Book

	err := c.db.QueryRow(ctx, query, req.GetBookId()).Scan(
		&resp.BookId,
		&resp.Isbn,
		&resp.Title,
		&resp.Cover,
		&resp.Author,
		&resp.Published,
		&resp.Pages,
		&resp.Status,
		&resp.CreatedAt,
		&resp.UpdatedAt,
		&resp.DeletedAt,
	)

	if err != nil {
		return nil, err
	}

	return &resp, nil
}

func (c *BookRepo) GetAll(ctx context.Context, req *book_service.GetListBookRequest) (resp *book_service.GetListBookResponse, err error) {

	resp = &book_service.GetListBookResponse{}

	var (
		query  string
		limit  = ""
		offset = " OFFSET 0 "
		params = make(map[string]interface{})
		filter = " AND TRUE"
		sort   = " ORDER BY created_at DESC"
	)

	query = `
		SELECT
			COUNT(*) OVER(),
			book_id,
			isbn,
			title,
			cover,
			author,
			CAST(published AS VARCHAR),
			pages,
			COALESCE(status, 0),
			TO_CHAR(created_at, 'YYYY-MM-DD HH24:MI:SS'),
			TO_CHAR(updated_at, 'YYYY-MM-DD HH24:MI:SS'),
			COALESCE(TO_CHAR(deleted_at, 'YYYY-MM-DD HH24:MI:SS'), '')
		FROM "books" WHERE COALESCE(TO_CHAR(deleted_at, 'YYYY-MM-DD HH24:MI:SS'), '')=''
	`

	if req.GetLimit() > 0 {
		limit = " LIMIT :limit"
		params["limit"] = req.Limit
	}

	if req.GetOffset() > 0 {
		offset = " OFFSET :offset"
		params["offset"] = req.Offset
	}

	if len(req.GetSearch()) > 0 {
		filter = " WHERE first_name||' '||last_name ILIKE" + "'%" + req.Search + "%'"
	}

	query += filter + sort + offset + limit

	query, args := helper.ReplaceQueryParams(query, params)
	rows, err := c.db.Query(ctx, query, args...)
	if err != nil {
		return resp, err
	}
	defer rows.Close()

	for rows.Next() {

		var book book_service.Book

		err := rows.Scan(
			&resp.Count,
			&book.BookId,
			&book.Isbn,
			&book.Title,
			&book.Cover,
			&book.Author,
			&book.Published,
			&book.Pages,
			&book.Status,
			&book.CreatedAt,
			&book.UpdatedAt,
			&book.DeletedAt,
		)

		if err != nil {
			return resp, err
		}

		resp.Books = append(resp.Books, &book)
	}

	return
}

func (c *BookRepo) Update(ctx context.Context, req *book_service.UpdateBook) (rowsAffected int64, err error) {

	query := `
			UPDATE
			    "books"
			SET
				isbn = $1,
				title = $2,
				cover = $3,
				author = $4,  
				published = $5,
				pages = $6,
				status = $7,
				updated_at = now()
			WHERE
				book_id = $8`

	result, err := c.db.Exec(ctx, query,
		req.Isbn,
		req.Title,
		req.Cover,
		req.Author,
		req.Published,
		req.Pages,
		req.Status,
		req.BookId,
	)
	if err != nil {
		return
	}

	return result.RowsAffected(), nil
}

func (c *BookRepo) UpdatePatch(ctx context.Context, req *models.UpdatePatchRequest) (rowsAffected int64, err error) {

	var (
		set   = " SET "
		ind   = 0
		query string
	)

	if len(req.Fields) == 0 {
		err = errors.New("no updates provided")
		return
	}

	fmt.Println(req)

	req.Fields["book_id"] = req.Id

	for key := range req.Fields {
		set += fmt.Sprintf(" %s = :%s ", key, key)
		if ind != len(req.Fields)-1 {
			set += ", "
		}
		ind++
	}

	query = `
		UPDATE
			"books"
	` + set + ` , updated_at = now()
		WHERE
			book_id = :book_id
	`

	query, args := helper.ReplaceQueryParams(query, req.Fields)

	result, err := c.db.Exec(ctx, query, args...)
	if err != nil {
		return
	}

	return result.RowsAffected(), err
}

func (c *BookRepo) Delete(ctx context.Context, req *book_service.BookPrimaryKey) error {

	query := `DELETE FROM "books" WHERE book_id = $1`

	_, err := c.db.Exec(ctx, query, req.BookId)

	if err != nil {
		return err
	}

	return nil
}
